CREATE TABLE users
(
    username            VARCHAR(100) NOT NULL,
    password            VARCHAR(100) NOT NULL,
    name                VARCHAR(100) NOT NULL,
    token               VARCHAR(100) NOT NULL,
    token_expired_at    NUMBER(38),
    PRIMARY KEY (username)
);