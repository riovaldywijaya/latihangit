CREATE TABLE addresses
(
    id              VARCHAR(100) NOT NULL,
    contact_id      VARCHAR(100) NOT NULL,
    street          VARCHAR(200),
    city            VARCHAR(100),
    province        VARCHAR(100),
    country         VARCHAR(100) NOT NULL,
    postal_code     VARCHAR(10),
    PRIMARY KEY (id),
    FOREIGN KEY (contact_id) REFERENCES contacts (id)
);