package belajar.restful.api.demojava.service;

import belajar.restful.api.demojava.model.entity.User;
import belajar.restful.api.demojava.model.repository.UserRepository;
import belajar.restful.api.demojava.model.request.RegisterUserRequest;
import belajar.restful.api.demojava.model.request.UpdateUserRequest;
import belajar.restful.api.demojava.model.response.UserResponse;
import belajar.restful.api.demojava.security.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ValidationService validationService;

    @Transactional
    public void register(RegisterUserRequest request){
        validationService.validate(request);

        if(userRepository.existsById(request.getUsername())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username already registered");
        }

        User user = new User();
        user.setName(request.getName());
        user.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        user.setUsername(request.getUsername());

        userRepository.save(user);
    }

    public UserResponse get(User user){
      return UserResponse.builder()
              .name(user.getName())
              .username(user.getUsername())
              .build();
    };

    public UserResponse update(User user, UpdateUserRequest request){
        validationService.validate(request);

        if(Objects.nonNull(request.getName())){
            user.setName(request.getName());
        }

        if(Objects.nonNull(request.getPassword())){
            user.setPassword(BCrypt.hashpw(request.getPassword(),BCrypt.gensalt()));
        }

        userRepository.save(user);

        return UserResponse.builder()
                .username(user.getUsername())
                .name(user.getName())
                .build();
    }

}
