package belajar.restful.api.demojava.controller;

import belajar.restful.api.demojava.model.entity.User;
import belajar.restful.api.demojava.model.request.CreateContactRequest;
import belajar.restful.api.demojava.model.request.SearchContactRequest;
import belajar.restful.api.demojava.model.request.UpdateContactRequest;
import belajar.restful.api.demojava.model.response.ContactResponse;
import belajar.restful.api.demojava.model.response.PagingResponse;
import belajar.restful.api.demojava.model.response.WebResponse;
import belajar.restful.api.demojava.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;

    @PostMapping(
            path = "/api/contacts",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> create(
            @RequestBody CreateContactRequest request,
            User user
    ){
        ContactResponse contactResponse = contactService.create(request, user);
        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @GetMapping(
            path = "/api/contacts/{contactId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> get(
            User user,
            @PathVariable("contactId") String contactId
    ) {
        ContactResponse contactResponse = contactService.get(user, contactId);

        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @PutMapping(
            path = "/api/contacts/{contactId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<ContactResponse> update(User user,
                                               @PathVariable("contactId") String contactId,
                                               @RequestBody UpdateContactRequest request){
        request.setId(contactId);
        ContactResponse contactResponse = contactService.update(user, request);

        return WebResponse.<ContactResponse>builder().data(contactResponse).build();
    }

    @DeleteMapping(
            path = "/api/contacts/{contactId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> delete(User user,
                                      @PathVariable("contactId") String id){
        contactService.delete(user,id);

        return WebResponse.<String>builder().data("OK").build();
    }

    @GetMapping(
            path = "/api/contacts",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<List<ContactResponse>> search (User user,
                                                      @RequestParam(value = "name", required = false) String name,
                                                      @RequestParam(value = "phone", required = false) String phone,
                                                      @RequestParam(value = "email", required = false) String email,
                                                      @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                      @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
        SearchContactRequest request = SearchContactRequest.builder()
                .name(name)
                .phone(phone)
                .email(email)
                .page(page)
                .size(size)
                .build();

        Page<ContactResponse> responses = contactService.search(user, request);

        return WebResponse.<List<ContactResponse>>builder()
                .data(responses.getContent())
                .paging(PagingResponse.builder()
                        .currentPage(responses.getNumber())
                        .totalPage(responses.getTotalPages())
                        .size(responses.getSize())
                        .build())
                .build();
    }
}
