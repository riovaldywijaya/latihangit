package belajar.restful.api.demojava.service;

import belajar.restful.api.demojava.model.entity.User;
import belajar.restful.api.demojava.model.repository.UserRepository;
import belajar.restful.api.demojava.model.request.LoginUserRequest;
import belajar.restful.api.demojava.model.response.TokenResponse;
import belajar.restful.api.demojava.security.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ValidationService validationService;

    public TokenResponse login(LoginUserRequest loginUserRequest){
        validationService.validate(loginUserRequest);

        User user = userRepository.findById(loginUserRequest.getUsername())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Username or Password is wrong"));

        if(BCrypt.checkpw(loginUserRequest.getPassword(), user.getPassword())){
            user.setToken(UUID.randomUUID().toString());
            user.setTokenExpiredAt(next30days());
            userRepository.save(user);

            return TokenResponse.builder()
                    .token(user.getToken())
                    .expiredAt(user.getTokenExpiredAt().toString())
                    .build();

        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Username or Password is wrong");
        }
    };

    @Transactional
    public void logout(User user){
        user.setToken(null);
        user.setTokenExpiredAt(null);

        userRepository.save(user);
    }

    private Long next30days(){
        return System.currentTimeMillis() + (1000 * 60 * 24 * 30);
    }
}
