package belajar.restful.api.demojava.controller;

import belajar.restful.api.demojava.WebConfiguration;
import belajar.restful.api.demojava.model.entity.User;
import belajar.restful.api.demojava.model.request.RegisterUserRequest;
import belajar.restful.api.demojava.resolver.UserArgumentResolver;
import belajar.restful.api.demojava.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {
    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

//    private final UserArgumentResolver userArgumentResolver = new UserArgumentResolver();

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
//                .setCustomArgumentResolvers(userArgumentResolver)
                .build();

    }

    @Test
    void testRegisterUserSuccess() throws Exception {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setUsername("test");
        request.setPassword("rahasia");
        request.setName("Test");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/users")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );

        Mockito.verify(userService).register(any(RegisterUserRequest.class));

    }

    @Test
    void testRegisterBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                MockMvcResultMatchers.status().isBadRequest()
        );
    }

    @Test
    void testRegisterDuplicate() throws Exception {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setUsername("test");
        request.setPassword("rahasia");
        request.setName("Test");

        Mockito.doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username already registered"))
                .when(userService).register(any(RegisterUserRequest.class));

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                MockMvcResultMatchers.status().isBadRequest()
        );

        Mockito.verify(userService).register(any(RegisterUserRequest.class));

    }

    @Test
    void getUserUnauthorized() throws Exception{
//        Mockito.doThrow(new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized"))
//                .when(userService).get(any(User.class));
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/users/current")
//                    .accept(MediaType.APPLICATION_JSON)
//                    .header("X-API-TOKEN","notfound")
//                )
//                .andExpectAll(
//                        MockMvcResultMatchers.status().isUnauthorized()
//                );
//
//        Mockito.verify(userService).get(any(User.class));
    }

    @Test
    void getUserUnauthorizedTokenNotSend() throws Exception{
//        mockMvc.perform(MockMvcRequestBuilders
//                        .get("/api/users/current")
//                        .accept(MediaType.APPLICATION_JSON)
//                )
//                .andExpect(
//                        MockMvcResultMatchers.status().isUnauthorized()
//                );

    }

    @Test
    void getUserSuccess() throws Exception{

    }

    @Test
    void getUserTokenExpired() throws Exception{

    }

    @Test
    void updateUserUnauthorized() throws Exception {

    }

    @Test
    void updateUserSuccess() throws Exception {

    }
}

/////////////////////////
//@SpringBootTest
//@AutoConfigureMockMvc
//public class UserControllerTest {
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @BeforeEach
//    public void setUp() {
//        userRepository.deleteAll();
//    }
//
//    @Test
//    void testRegisterUserSuccess() throws Exception {
//        RegisterUserRequest request = new RegisterUserRequest();
//        request.setUsername("test");
//        request.setPassword("rahasia");
//        request.setName("Test");
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .post("/api/users")
//                .accept(MediaType.APPLICATION_JSON)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(request))
//        ).andExpect(
//                MockMvcResultMatchers.status().isOk()
//        )
//        .andDo(result -> {
//            WebResponse<String> response = objectMapper.readValue(
//                    result.getResponse().getContentAsString(),
//                    new TypeReference<>() {
//            });
//
//            Assertions.assertEquals("OK", response.getData());
//        });
//
//    }
//}
