package belajar.restful.api.demojava.controller;

import belajar.restful.api.demojava.model.request.LoginUserRequest;
import belajar.restful.api.demojava.service.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class AuthControllerTest {
    @InjectMocks
    private AuthController authController;

    @Mock
    private AuthService authService;

    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .build();
    }

    @Test
    void successLogin() throws Exception {
        LoginUserRequest loginUserRequest = LoginUserRequest.builder()
                .username("rio")
                .password("rahasia")
                .build();

        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/auth/login")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(loginUserRequest))
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );

        Mockito.verify(authService).login(loginUserRequest);
    }

    @Test
    void failLogin() throws Exception {
//        LoginUserRequest loginUserRequest = LoginUserRequest.builder()
//                .username("rio")
//                .password("rahasia")
//                .build();
//
//        Mockito.when(authService.login(any(LoginUserRequest.class)))
//                .thenThrow(new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Username or Password is wrong"));
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .post("/api/auth/login")
//                    .accept(MediaType.APPLICATION_JSON)
//                    .contentType(MediaType.APPLICATION_JSON)
//                    .content(objectMapper.writeValueAsString(loginUserRequest))
//        ).andExpect(
//                MockMvcResultMatchers.status().isUnauthorized()
//        );
//
//        Mockito.verify(authService).login(loginUserRequest);
    }

    @Test
    void logoutFailed() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders
//                .delete("/api/auth/logout")
//                    .accept(MediaType.APPLICATION_JSON)
//        ).andExpect(
//            MockMvcResultMatchers.status().isUnauthorized()
//        );

    }

    @Test
    void logoutSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/auth/logout")
                    .accept(MediaType.APPLICATION_JSON)
                    .header("X-API-TOKEN","test")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        );
    }
}
