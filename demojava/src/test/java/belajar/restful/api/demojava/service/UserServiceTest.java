package belajar.restful.api.demojava.service;

import belajar.restful.api.demojava.model.repository.UserRepository;
import belajar.restful.api.demojava.model.request.RegisterUserRequest;
import jakarta.validation.ConstraintViolationException;
import jakarta.xml.bind.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ValidationService validationService;

    @BeforeEach
    void setUp() {

    }

    @Test
    void successRegister() throws Exception {
        RegisterUserRequest request = RegisterUserRequest.builder()
                .username("rio")
                .password("rahasia")
                .name("riovaldy wijaya")
                .build();

        Mockito.when(userRepository.existsById(request.getUsername()))
                .thenReturn(Boolean.FALSE);

        Assertions.assertDoesNotThrow(() -> userService.register(request));

    }

    @Test
    void failRegisterDuplicate() throws Exception {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setUsername("testuser");
        request.setPassword("password123");
        request.setName("Test User");

        Mockito.when(userRepository.existsById(request.getUsername())).thenReturn(true);

        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> {
            userService.register(request);
        });

        Assertions.assertEquals("Username already registered", exception.getReason());

        Mockito.verify(validationService).validate(request);

    }

    @Test
    void failRegisterValidation() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setUsername("testuser");
        request.setPassword("password123");
        request.setName("Test User");

        Mockito.doThrow(new ConstraintViolationException("Validation failed", null))
                .when(validationService)
                .validate(request);

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            userService.register(request);
        });

        Mockito.verify(validationService).validate(request);
    }

    @Test
    void successGetUser() throws Exception {

    }

    @Test
    void failGetUser() throws Exception {

    }

    @Test
    void successUpdate() throws Exception {

    }
}

